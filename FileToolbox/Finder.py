#! python3
# -*- coding: utf-8 -*-

from os import walk, path
from HIP.FileToolbox.URICleaner import CleanPath as cp

class Finder:

	@staticmethod
	def find_files_named(filename, path_name, alias=""):
		"""
		Finds files matching filename in the given path
		:param filename: String : Filename
		:param path_name:  String Path  to look for files
		:param alias:  Alias of the file, (for debugging purpose). Default is ""
		:return: array of found file paths
		"""
		paths = []
		for (dir_path, dir_names, file_names) in walk(path_name):
			if filename in file_names:
				new_file_path = cp.clean(cp.path_for_file_in_dir(path_name, filename))
				paths.append(new_file_path)
			else:
				for dir in dir_names:
					new_dir_path = cp.path_by_appending_dir(path_name, dir)
					paths += Finder.find_files_named(filename, new_dir_path, alias)
				break
		return paths

	@staticmethod
	def find_files_with_prefix(prefix, path_name, alias=""):
		"""
		Finds files with filename matching the given prefix
		:param prefix: String : Filename
		:param path_name:  String Path  to look for files
		:param alias:  Alias of the file, (for debugging purpose). Default is ""
		:return: array of found file paths
		"""
		paths = []
		for (dir_path, dir_names, file_names) in walk(path_name):
			for filename in file_names:
				if filename.startswith(prefix):
					new_file_path = cp.path_for_file_in_dir(path_name, filename)
					paths.append(new_file_path)

			for dir in dir_names:
				new_dir_path = cp.path_by_appending_dir(path_name, dir)
				paths += Finder.find_files_with_prefix(prefix, path_name + new_dir_path, alias)
			break
		return paths



