#! python3
# -*- coding: utf-8 -*-

from os import path

class CleanPath:
	"""
	Tools for easy use of file and directory related URIs
	"""

	@staticmethod
	def path_for_file_in_dir(directory, filename, check_if_target_exists = False):
		"""
		This method returns the name of a given file in a given directory
		:param directory:  String, directory to look for file into
		:param filename:  String, filename
		:param check_if_target_exists: Bool (default=False) Force the method to return None if the target
			file doesn't exist
		:return: String new file path, eventuallt None according to check_if_target_exists param
		"""
		file_path = ""
		if directory[-1] == path.sep:
			file_path = directory + filename
		else:
			file_path = directory + path.sep + filename

		if check_if_target_exists:
			if not path.isfile(file_path):
				return None

		return file_path

	@staticmethod
	def path_by_appending_dir(given_path, directory):
		"""
		Adds a given directory name to a given path
		:param given_path: String  The root path
		:param directory: String The directory to add
		:return: String The new path
		"""
		new_path = ""
		if given_path[-1] == path.sep:
			new_path = given_path + directory
		else:
			new_path = given_path + path.sep + directory

		if new_path[-1] != path.sep:
			new_path = new_path + path.sep

		return new_path

	@staticmethod
	def clean(file_path):
		"""
		This method cleans multiples directory separators from a given path
		:param file_path: The file path
		:return: String
		"""
		sep2 = path.sep + path.sep
		sep3 = sep2 + path.sep
		p = file_path.replace(sep3, path.sep)
		p = p.replace(sep2, path.sep)

		return p





