#! python3
# -*- coding: utf-8 -*-

from os import path, rename as os_rename, mkdir, unlink as os_remove, rmdir
from HIP.FileToolbox.Finder import Finder
from HIP.FileToolbox.URICleaner import CleanPath as cp
from shutil import copy2 as os_copy, rmtree
from datetime import datetime
from hashlib import md5

from enum import Enum
import json

BACKUP_PREFIX = "__backup__."
TRASH_PREFIX = "__trash__."
BACKUP_FOLDER = "__BACKUP__" + path.sep
BACKUP_INDEX_FILE = "__bckp_index.json"


class BackupMethod(Enum):
	COPY = "copy"
	RENAME = "remove"
	INDEX_BASED = "index_based"

class Backup:

	@staticmethod
	def default_backup_index_structure(root=""):
		"""
		Returns the default backup structure
		:param root: String The bas root directory to establish the structure from
		:return: dictionary representing the data structure (to be JSONified)
		"""
		return {
			"backup_date" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
			"root_directory" : root,
			"files": {},
			"removable_directories": []
		}


	@staticmethod
	def check_if_index_exists(root=""):
		""" Checks if the project root backup index exists, if it does not, creates it and the required data structure

		:param root The backup root directory
		return String The path of the found index file.
		"""

		root = cp.clean(root)
		full_backup_path = cp.path_by_appending_dir(root, BACKUP_FOLDER)
		backup_index_file = cp.path_for_file_in_dir(full_backup_path, BACKUP_INDEX_FILE)
		if path.isdir(full_backup_path):
			if path.isfile(backup_index_file):
				return backup_index_file
		else:
			mkdir(full_backup_path)

		structure = Backup.default_backup_index_structure(root)
		json_string = json.dumps(structure, indent=4, sort_keys=True)
		print(json_string)

		Backup.dump_backup_data_to_index(backup_index_file, structure)

		return cp.clean(backup_index_file)

	@staticmethod
	def backup_file(file_path, method=BackupMethod.INDEX_BASED, prefix=BACKUP_PREFIX, index_root="."):
		""" Creates a backup of the given file, in the same directory using the given prefix

			:param file_path: Sring The file to be backed up
			:param method: BackupMethod The method to use for backups (rename of copy, default:rename)
			:param prefix: String The prefix of the backup file (default __backup__.), not used for index based backup
			:param index_root String For index_based backup, the root folder for the backup index and structure
			:return:
		"""

		new_path = cp.clean(path.dirname(file_path) + path.sep + prefix + path.basename(file_path))
		if method == BackupMethod.RENAME:
			os_rename(file_path, new_path)
		elif method == BackupMethod.COPY:
			os_copy(file_path, new_path)
		elif method == BackupMethod.INDEX_BASED:
			Backup.index_based_backup(cp.clean(file_path), index_root)


	# TODO: Create a new method that will take an array of files instead of just one, in order to limit I/O
	@staticmethod
	def index_based_backup(file_path, index_root):
		""" This method backup a given file using index based method

		:param file_path: String The file to be backed up
		:param index_root: String The root folder for the backup action
		:return:
		"""
		backup_file = Backup.check_if_index_exists(index_root + path.sep)
		backup_index_data = {}
		with open(backup_file, "r") as json_file:
			backup_index_data = json.load(json_file)

			file_already_present, filehash = Backup.file_is_in_backup_index_data(file_path, backup_index_data, index_root)

			if not file_already_present:
				backup_index_data, backup_filename = Backup.updated_index_object(backup_index_data, filehash, file_path)
				os_copy(file_path, backup_filename)

		Backup.dump_backup_data_to_index(backup_file, backup_index_data)

	@staticmethod
	def dump_backup_data_to_index(index_file, backup_index_data):
		""" This method is ued to dump the given data to the index

		:param index_file: String index file
		:param backup_index_data:  data to be dumped
		:return:
		"""

		with open(index_file, "w") as new_json_file:
			json.dump(backup_index_data, new_json_file, sort_keys=True, indent=4)

	@staticmethod
	def updated_index_object(index_object, file_hash, filename):
		""" Updates the backup index data object

		:param index_object: The dictionary to be updated
		:param file_hash: The backup file hash
		:param filename: The real file name
		:return: Dictionary, String : Updated index object and filename of the newly added file as a backup file
		"""

		if "root_directory" not in index_object:
			print("ERROR - NO root directory in backup index file !")
			exit(10)

		root_path = index_object["root_directory"]
		file_entry = "files"
		if file_entry not in index_object:
			index_object[file_entry] = {}

		backup_file_name = root_path + BACKUP_FOLDER + file_hash
		index_object[file_entry][file_hash] = {
			"initial_path": filename,
			"backup_file": backup_file_name
		}

		return index_object, backup_file_name

	@staticmethod
	def file_is_in_backup_index_data(file_path, backup_index_data, index_root):
		"""
		Checks if the given file is in the backup index

		:param file_path: String The filename to check
		:param backup_index_data: The dictionary containing backup index
		:param index_root: The backup index root path
		:return: file_already_present:Bool, filehash: String
		"""
		# checks if the file to be backed up is already in the dictionary
		# look for the file hash
		file_already_present = False
		short_filename = file_path[len(index_root) + 1: len(file_path)]
		filehash = md5(short_filename.encode()).hexdigest()

		if (filehash in backup_index_data["files"]):
			file_already_present = True

		return file_already_present, filehash

	@staticmethod
	def add_directories_to_removable(index_root, directories):
		""" Adds the given directories to the removable directories index

		:param index_root: String Backup index root folder
		:param directories: List of String Directories to be added
		:return:
		"""

		backup_file = Backup.check_if_index_exists(index_root + path.sep)
		removables = []
		with open(backup_file, "r") as json_file:
			backup_index_data = json.load(json_file)
			if "removable_directories" in backup_index_data:
				removables = backup_index_data["removable_directories"]

			removables = list(set().union(removables, directories))

		backup_index_data["removable_directories"] = removables
		Backup.dump_backup_data_to_index(backup_file, backup_index_data)


	@staticmethod
	def restore_all_backups(directory, with_index=False, prefix=BACKUP_PREFIX, use_trash = False):
		"""
		Restores all backups in a given directory.
		:param directory: Directory to look for backups
		:param prefix: backups prefix (default)
		:param use_trash: True if you want to keep a trash copy of the file before backup
		:return:
		"""
		if with_index:
			Backup.restore_all_backups_from_index(directory)
		else:
			Backup.restore_all_backups_without_index(directory, prefix, use_trash)

	@staticmethod
	def restore_all_backups_from_index(directory):
		""" Restores all files backed up for the given directory index

		:param directory: String Path to the directory containing index
		:return:
		"""

		if not path.isdir(directory):
			print(f"[ERROR] Cannot find index, ({directory}) does not seem to be a directory'")
			exit(4)

		if directory[-1] != path.sep:
			directory = directory + path.sep

		index_filepath = directory + BACKUP_FOLDER + BACKUP_INDEX_FILE
		if not path.isfile(index_filepath):
			print(f"[ERROR] Cannot find index at {index_filepath} No such file !")

		# Restore files
		backup_index = {}
		with open(index_filepath, "r") as index_file_handler:
			backup_index = json.load(index_file_handler)
			backup_index = Backup.restore_files_and_cleanup(backup_index)

			# Removing useless directories
			backup_index = Backup.remove_useless_dirs(backup_index)

		Backup.dump_backup_data_to_index(index_filepath, backup_index)

	@staticmethod
	def restore_files_and_cleanup(backup_index_data):
		""" Restores files using backup index data and remove backup files

		:param backup_index_data: Object Backup index data dictionary
		:return: Dict Updated Backup index data dictionary
		"""

		restored_files_hash = []

		if "files" in backup_index_data:
			files = backup_index_data["files"]

			restored_files_counter = 0
			file_to_restore_counter = len(files)

			for md5_hash in files:
				file_dic = files[md5_hash]
				success = Backup.restore_file_from_file_dictionary(file_dic)
				if success:
					restored_files_hash.append(md5_hash)
					restored_files_counter += 1

			for restored_hash in restored_files_hash:
				del backup_index_data["files"][restored_hash]

			print(f"[RESTORE]  Successfully restored {restored_files_counter} files  (out of {file_to_restore_counter})")
			backup_index_data["files"] = files

		return backup_index_data

	@staticmethod
	def remove_useless_dirs(backup_index_data):
		""" Removes files and directory form filesystem

		:param backup_index_data: Object Backup index data dictionary
		:return: Dict Updated Backup index data dictionary
		"""
		if "removable_directories" in backup_index_data:
			removable_dirs = backup_index_data["removable_directories"]
			removable_dirs = sorted(removable_dirs, key=str.lower, reverse=True)

			for dir in removable_dirs:
				print(f"[CLEAN UP] Removing useless directory : {dir}")
				rmtree(dir)

			backup_index_data["removable_directories"] = []

		return backup_index_data




	@staticmethod
	def restore_file_from_file_dictionary(file_dictionary):
		"""
		This method restores a backed up file from a entry of teh index files' dictionary
		:param file_dictionary:  The index entry
		:return: Bool True if restoration is OK
		"""
		intial_path = file_dictionary["initial_path"]
		backup_file = file_dictionary["backup_file"]
		try:
			if path.isfile(intial_path):
				os_remove(intial_path)

			os_rename(backup_file, intial_path)
			return True
		except (FileNotFoundError):
			print(f"[RESTORE][KO]\t->Can't find {backup_file}")
			return False


	@staticmethod
	def restore_all_backups_without_index(directory, prefix=BACKUP_PREFIX, use_trash = False):
		"""
		Restores all backups in a given directory.
		:param directory: Directory to look for backups
		:param prefix: backups prefix (default)
		:param use_trash: True if you want to keep a trash copy of the file before backup
		:return:
		"""
		files = Finder.find_files_with_prefix(prefix, directory, alias="Backup")

		for file in files:
			base_name = path.basename(file)
			pf_len = len(prefix)
			former_file_name =path.dirname(file) + path.sep + base_name[ pf_len:len(base_name)]
			restored_path = path.dirname(file) + path.sep + former_file_name

			if use_trash:
				trash_path = path.dirname(file) + path.sep + TRASH_PREFIX + base_name
				# Preparing deletion
				if path.isfile(former_file_name):
					os_rename(former_file_name, trash_path)

			print(f"File will be restored to : {restored_path}")
			os_rename(file, former_file_name)

