#! python3
# -*- coding: utf-8 -*-

from HIP.FileToolbox.Backup import Backup, BackupMethod
from HIP.FileToolbox.URICleaner import CleanPath as cp

import fileinput
import re
from typing import List
from os import walk, sep as os_sep, path, mkdir
from shutil import copy2 as os_copy


class ReplaceTuple:

	def __init__(self, search_regex, replacement):
		self.search = search_regex
		self.target = replacement


Replacements = List[ReplaceTuple]


class Replacer:

	@staticmethod
	def replace_in_file(replacements: Replacements, file):
		"""
		Replaces the content of a given file matching a list of ReplacementTuples and returning the updated results
		:param replacements:  Array of ReplacementTuple
		:param file:  File to be read
		:return: new contents as a string
		"""

		new_content = ""

		for n, line in enumerate(fileinput.input(file, inplace=False), start=1):
			processed_line = False
			for rTuple in replacements:
				if not processed_line:
					regex = rTuple.search
					subst = rTuple.target
					result, change_count = re.subn(regex, subst, line, 0)

					if change_count > 0:
						processed_line = True
						new_content += result
						# print(f" [line: {n}] Replaced {regex} by {subst}")

			if processed_line is False:
				new_content += line

		return new_content
	# EOF replace_in_file

	@staticmethod
	def swap_files(work_directory, replacement_files_directory, use_backup = False, backup_root_directory=None):
		"""

		:param work_directory: String The directory to analyse for file swapping
		:param replacement_files_directory: String The directory where to look for files
		:param use_backup: Bool Tells if backup Must be made
		:param backup_root_directory: Directory to store index based backup, default is None, if None, will use work_dierctory
		:return:
		"""

		# 1 - get full path of all files in the replacement directory
		flavor_files = []
		for (dir_path, dir_names, file_names) in walk(replacement_files_directory):
			for file in file_names:
				shrunk_root_path = dir_path[len(replacement_files_directory):len(dir_path)]
				flavor_files.append(shrunk_root_path + os_sep + file)

		counter = "No"

		if len(flavor_files) > 0:
			counter = f"{len(flavor_files)}"

		print(f"\t# {counter} Flavor files found")
		for f in flavor_files:
			print(f"\t\t - {f}")

		# 2 - for every file in the previous list, checks if it matches with the work directory
		created_directories = []
		for file in flavor_files:
			original_file = cp.clean(work_directory + file)
			flavor_files = cp.clean(replacement_files_directory + file)

			# 3 - make backups of all files that will be replaced
			if use_backup:
				if backup_root_directory is None:
					backup_root_directory = work_directory

				if path.isfile(original_file):
					Backup.backup_file(original_file, method=BackupMethod.INDEX_BASED, index_root=backup_root_directory)

		# 4 - update files preserving directory structure
			newly_created_directories = Replacer.create_directory_structure_for_file(original_file)
			created_directories = list(set().union(created_directories, newly_created_directories))

			os_copy(flavor_files, original_file)

		from pprint import pprint
		pprint(created_directories)
		Backup.add_directories_to_removable(backup_root_directory, created_directories)


	@staticmethod
	def create_directory_structure_for_file(filename):
		"""
		This method created the required directory structure in order to create the given file
		:param filename:  String, filename
		:return: List of created directories; empty list if nothing was done
		"""

		split_dirs = filename.split(path.sep)
		split_dirs.pop()                   # Removing filename from directories

		current_path = ""
		path_element_of_from_root = 0

		for element in split_dirs:
			if len(element) != 0:
				lookup_path = current_path + element
				if path.isdir(lookup_path):
					current_path = lookup_path + path.sep
				else:
					break      # Breaking loop when directory doesn't exist
			else:
				current_path += path.sep

			path_element_of_from_root += 1

		# Now create the missing directory structure
		elements_to_be_created = split_dirs[path_element_of_from_root:len(split_dirs)]

		created_directories = []
		for element in elements_to_be_created:
			current_path += element + os_sep
			created_directories.append(current_path)
			mkdir(current_path)

		return created_directories


if __name__ == '__main__':

	print("Testing file Replacer")
	Replacer.swap_files(
		"/Users/davidbeja/Projects/TF1/miami-front-qml",
		"/Users/davidbeja/Projects/TF1/miami-front-qml/flavors/dev"
	)

	print("Restoring backups")
	Backup.restore_all_backups("/Users/davidbeja/Projects/TF1/miami-front-qml")
