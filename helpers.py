from colorama import Fore, Style

class 	KeyPath:
    	
	def __init__(self, path, separator = "."):
		""" Class initializer
		:param path: String the keypath string
		:param separator:  String The keypath separator
		"""
		self.path = path
		self.separator = separator


	def is_in(self, o):
		""" Tells if a given key path can be found in a given object
		:param o:
		:return:
		"""
		return KeyPath.exists(o, self.path, key_separator=self.separator)

	def follow_into(self, o):
		""" Look into the given object to find a value at the given keypath
		:param o: Obj The iterable object to look into
		:return: (Obj, bool) Found object, Bool telling if object was found
		"""
		return KeyPath.find_object_at(o, self.path, key_separator=self.separator)
		pass


	@classmethod
	def find_object_at(cls, o,  keypath_string, key_separator="."):
		"""  Returns an object found at th given keypath, none otherwise

		:param o: The iterable object to look into
		:param keypath_string: Str The keypath represented by a string
		:param key_separator: Str  The keypath separator as a string
		:return: (Object, Bool) The object at the given keypath, None if not Found  ; A boolean telling if the object was found
		"""
		if o is None:
			return None, False

		if keypath_string is None or len(keypath_string) == 0:
			return o, True

		split = keypath_string.split(key_separator, maxsplit=1)
		if len(split) == 2:
			key = split[0]
			sequel = split[1]

			key_found, result_object = KeyPath.find_key_in_obj(key, o)
			if key_found:
				return KeyPath.find_object_at(result_object, sequel, key_separator=key_separator)
		elif len(split) > 0:
			key = split[0]
			key_found, result_object = KeyPath.find_key_in_obj(key, o)
			return result_object, True

		return None, False


	@classmethod
	def exists(cls, o, keypath_string, key_separator="."):
		""" Tells if the given keypath exists in a given object

		:param o: The object to search keypath into
		:param keypath_string: The keypath representinfg by a sipmple String
		:param key_separator: The key separator in keypath
		:return: Bool or Object
		"""

		if o is None or keypath_string is None:
			return False

		split = keypath_string.split(key_separator, maxsplit=1)
		if len(split) == 2:
			key = split[0]
			sequel = split[1]

			key_found, result_object = KeyPath.find_key_in_obj(key, o)

			if key_found:
				return KeyPath.exists(result_object, sequel)
		elif len(split) > 0:
			key = split[0]
			key_found, result_object = KeyPath.find_key_in_obj(key, o)
			return key_found

		return False

	@staticmethod
	def find_key_in_obj(key, obj):
		""" Test if the key can be found is an object as a string or an iny

		:param key: string, the key to look for
		:param obj: the object to search into
		:return: (Bool, value)
		"""
		obj_type = type(obj)
		key_int_value = 0
		key_can_be_int = False

		try:
			test = int(key)
			if isinstance(test, int):
				key_int_value = test
				key_can_be_int = True
		except ValueError:
			key_can_be_int = False
			pass

		if obj_type in [list, tuple]:
			if key_can_be_int and key_int_value < len(obj):
				# Key as an index found in obj
				return True, obj[key_int_value]
			else:
				# index nt found
				return False, None
		elif obj_type is dict:
			if key in obj:
				# key found in its natural type in dictionary
				return True, obj[key]
			elif key_can_be_int and key_int_value in obj:
				# key found as an int in the dictionary
				return True, obj[key_int_value]
			else:
				# key not found in the dictionary
				return False, None
		else:
			# Key not found or object is not iterable
			return False, None

class Tools:

	@classmethod
	def key_in_dict( cls, key, dictionary, default_value=None ):
		"""
		Method allowing to set a value only if the key is found in the given dictionary
		param key: The key to look for
		param dictionary:  The dictionary to look into
		param default_value:  The default value to be sent if key is not found
		return:  Extracted value or default
		"""

		if key in dictionary:
			return dictionary[key]
		else:
			return default_value


class Text:

	@classmethod
	def bright(cls, text):
		"""
		Simply turns the given text to bright mode using colorama
		:param text: text
		:return: brightened text
		"""
		return Style.BRIGHT + text + Style.RESET_ALL

	@classmethod
	def dim(cls, text):
		"""
			Simply turns the given text to dim mode using colorama
			:param text: text
			:return: dimmed text
		"""

		return Style.DIM + text + Style.RESET_ALL


if __name__ == "__main__":
	print("Test case")
	a = { "a": "a", "b" : "b", }
