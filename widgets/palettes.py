from PySide2.QtGui import QColor


class DefaultPalette:
	"""
	Default palette for widgets
	"""
	LOW_TEXT_COLOR = QColor(127, 129, 136)
	TEXT_COLOR = QColor(56, 69, 79)

	BLUE = QColor(22, 62, 85)
	GREEN = QColor(0, 110, 47)
	RED = QColor(211, 35, 24)
