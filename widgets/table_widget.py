from PySide2.QtWidgets import QVBoxLayout, QWidget, QTableWidget, QTableWidgetItem


class TableWidget(QWidget):

	def __init__(self):
		super().__init__()
		self.title = "Table Widget"

		self.left = 50
		self.top = 50
		self.width = 800
		self.height = 600

		self.layout = None
		self.table_widget = QTableWidget()

		self.init_main_ui()

	def set_widget_title(self, s_title):
		self.title = s_title

	def init_main_ui(self):
		self.setWindowTitle(self.title)
		self.setGeometry(self.left, self.top, self.width, self.height)

		self.create_table()

		# Add box layout, add table to box layout and add box layout to widget
		self.layout = QVBoxLayout()
		self.layout.addWidget(self.table_widget)
		self.setLayout(self.layout)

		# Show widget
		self.show()

	def create_table(self):
		# Create table
		self.table_widget = QTableWidget()
		self.table_widget.setRowCount(0)

		self.table_widget.move(0, 0)

	def add_row(self, data, colors=None):
		"""
		Adds a new log row
		:param data: Data to be added
		:param colors: (optionnal) Colors for the given cell foreground
		:return:
		"""
		new_row_index = self.table_widget.rowCount()
		self.table_widget.setRowCount(new_row_index + 1)

		current_cell_index = 0
		for cell_content in data:
			if current_cell_index < self.table_widget.columnCount():
				self.table_widget.setItem(new_row_index, current_cell_index, QTableWidgetItem(cell_content))
				if colors is not None:
					color = colors[current_cell_index]
					self.table_widget.item(new_row_index, current_cell_index).setTextColor(color)

			current_cell_index += 1

