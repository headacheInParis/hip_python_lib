from HIP.widgets.table_widget import TableWidget
from HIP.widgets.palettes import DefaultPalette

from PySide2.QtCore import Slot
from PySide2.QtWidgets import QHeaderView
from datetime import datetime


class HTTPLogWidget(TableWidget):

	def __init__(self):
		super().__init__()
		self.set_widget_title("HTTP Log Widget")

	def create_table(self):
		super().create_table()

		# Setup titles
		titles = ["Date", "Code", "Method", "URI", "UA"]
		self.table_widget.setColumnCount(len(titles))
		self.table_widget.setHorizontalHeaderLabels(titles)
		header = self.table_widget.horizontalHeader()
		header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
		header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
		header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
		header.setSectionResizeMode(3, QHeaderView.Stretch)
		header.setSectionResizeMode(3, QHeaderView.ResizeToContents)
		header.setSectionsMovable(True)

		# table selection change
		self.table_widget.doubleClicked.connect(self.on_click)

	@Slot()
	def on_click(self):
		print("\n")
		for currentQTableWidgetItem in self.table_widget.selectedItems():
			print(currentQTableWidgetItem.row(), currentQTableWidgetItem.column(), currentQTableWidgetItem.text())

	def add_log_row(self, data, colors=None):
		"""
		Adds a new log row
		:param data: Data to be added
		:param colors: Colors for the given cell foreground
		:return:
		"""
		super().add_row(data, colors)

	@classmethod
	def color_for_status_code(cls, status_code):
		"""
		Returns a choosen color for a given HTTP status code
		:param status_code: The status code to read
		:return: The identifying color as QColor
		"""
		code_color = [
			DefaultPalette.LOW_TEXT_COLOR,  # 0-99
			DefaultPalette.TEXT_COLOR,  # 100-199
			DefaultPalette.GREEN,  # 200-299
			DefaultPalette.BLUE,  # 300-399
			DefaultPalette.RED,  # 400-499
			DefaultPalette.RED  # 500-599
		]

		value = int((status_code - (status_code % 100)) / 100)
		if value > len(code_color):
			value = 1

		return code_color[value]

	def add_log(self, timestamp=None, verb="GET", status=200, url="/", ua=""):
		"""
		Adds a row in the HTTP Log
		:param timestamp: String with format '%Y-%m-%dT%H:%M:%S.%fZ')  -- Default : None, understood as now
		:param verb: String
		:param status: int status code
		:param url: string called URL
		:param ua: String user agent
		:return:
		"""

		if timestamp is None:
			timestamp = datetime.strptime(datetime.now(), '%Y-%m-%dT%H:%M:%S.%fZ')

		datetime_object = datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%fZ')
		time_string = "{}".format(datetime_object.strftime("%H:%M:%S"))
		row = [time_string, f"{status}", verb, url, ua]
		colors = [
			DefaultPalette.LOW_TEXT_COLOR,
			HTTPLogWidget.color_for_status_code(status_code=status),
			DefaultPalette.TEXT_COLOR,
			DefaultPalette.TEXT_COLOR,
			DefaultPalette.TEXT_COLOR
		]

		self.add_log_row(row, colors)
