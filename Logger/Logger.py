from colorama import Fore, Style, Back
from random import  random, choice
from functools import reduce

class Logger:

    log: str = ""
    output_to_console = False
    eol_character = "\n"
    prefix_color = Back.RESET
    key = None

    console_prefix = ""

    def randomize_prefix_color(self):
        colors = [
            Back.RED,
            Back.GREEN ,
            Back.YELLOW,
            Back.BLUE ,
            Back.MAGENTA ,
            Back.CYAN ,
            Back.LIGHTBLACK_EX,
            Back.LIGHTRED_EX,
            Back.LIGHTGREEN_EX,
            Back.LIGHTYELLOW_EX,
            Back.LIGHTBLUE_EX,
            Back.LIGHTMAGENTA_EX ,
            Back.LIGHTCYAN_EX
        ]
        self.prefix_color = choice(colors)



    def __init__(self):
        self.randomize_prefix_color()

    def add(self, contents: str, force_console_output=False):
        """ Adds the given contents to the log, can display the current addition

        :param contents: The contents to be added
        :param force_console_output: tells if the contents newly added contents should be displayed on the console
        :return: None
        """
        self.log += contents + self.eol_character
        if self.output_to_console or force_console_output:
            output_lines = contents.split("\n")
            prefix = self.prefix_color + self.console_prefix + Back.RESET + " "
            prefixed_outputs = list( map(lambda t: prefix + t, output_lines) )
            prefixed_contents = reduce( (lambda t1, t2: t1 + "\n" + t2 ), prefixed_outputs )

            print(prefixed_contents)

    def clear(self):
        self.log = ""
