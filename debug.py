from colorama import Fore, Back, Style


class D:

	@classmethod
	def print(cls, stri, shouldPrintFlag = True, color = "YELLOW"):
		c = Fore.YELLOW

		if color == "BLUE":
			c = Fore.BLUE
		elif color == "RED":
			c = Fore.RED
		elif color == "GREEN":
			c = Fore.GREEN

		str = c + Style.BRIGHT +  "[DEBUG] " + Style.RESET_ALL + c + stri + Style.RESET_ALL
		D.cPrint(str, shouldPrintFlag)

	@classmethod
	def cPrint( cls, string, shouldPrintFlag = True ):

		"""
		Conditionnaly prints the contents
		:param string: The string to be printd out
		:param shouldPrintFlag: Flag telling if th function should print anything
		"""

		if (shouldPrintFlag):
			print(string)

		pass


