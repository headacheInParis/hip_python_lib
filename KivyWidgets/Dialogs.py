from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.factory import Factory as F
from kivy.properties import ObjectProperty
from kivy.uix.popup import Popup
import os
from pprint import pprint


class FilechooserPopupDelegate:

    selected_path = ""
    selected_items = []

    def on_file_selected_callback(self):
        print(f"ERROR : on_file_selected_callback MUST be overloaded")

    def on_folder_selected_callback(self):
        print(f"ERROR : on_folder_selected_callback MUST be overloaded")


class LoadDialog(FloatLayout):
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)


    def __init__(self, ** kwargs):
        super(LoadDialog, self).__init__(** kwargs)
        self.init_widget()
        pprint(self.ids)

    def init_widget(self, *args):
        fc = self.ids['filechooser']
        fc.bind(on_entry_added=self.update_file_list_entry)
        fc.bind(on_subentry_to_entry=self.update_file_list_entry)

    def update_file_list_entry(self, file_chooser, file_list_entry, *args):
        file_list_entry.ids['filename'].text = "       "+file_list_entry.ids['filename'].text


class SaveDialog(FloatLayout):
    save = ObjectProperty(None)
    text_input = ObjectProperty(None)
    cancel = ObjectProperty(None)


class FilechooserPopup(FloatLayout):
    loadfile = ObjectProperty(None)
    savefile = ObjectProperty(None)
    text_input = ObjectProperty(None)
    delegate: FilechooserPopupDelegate = None

    default_path = "/"
    select_folders = False


    def dismiss_popup(self):
        self._popup.dismiss()

    def show_load(self):
        load_dialog = LoadDialog(load=self.load,
                                 cancel=self.dismiss_popup,
                                )

        load_dialog.ids.filechooser.path=self.default_path
        load_dialog.ids.filechooser.dirselect = self.select_folders

        self._popup = Popup(title="Charger ...",
                            content=load_dialog,
                            size_hint=(0.9, 0.9),
                            )
        self._popup.open()

    def show_save(self):
        content = SaveDialog(save=self.save, cancel=self.dismiss_popup)
        self._popup = Popup(title="Save file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def load(self, path, filename):
        if self.delegate is not None:
            self.delegate.selected_path = path
            self.delegate.selected_items = filename

            if self.select_folders:
                self.delegate.on_folder_selected_callback()
            else:
                if filename is None or len(filename) == 0:
                    print(f"Invalid choice, no item selected")
                    return
                self.delegate.on_file_selected_callback()
        else:
            print(f"Delegate is not set, no action triggered")

        self.dismiss_popup()

            # if os.path.isfile(file_path):
            #     with open(file_path) as stream:
            #         self.text_input.text = stream.read()
            #         self.dismiss_popup()
            # else:
            #     print(f"Cannot open file : {path}")


    def save(self, path, filename):
        with open(os.path.join(path, filename), 'w') as stream:
            stream.write(self.text_input.text)

        self.dismiss_popup()


class Dialogs(App):
    pass


F.register('FilechooserPopup', cls=FilechooserPopup)
F.register('LoadDialog', cls=LoadDialog)
F.register('SaveDialog', cls=SaveDialog)

from kivy.lang import  Builder
Builder.load_file('./HIP/kivyWidgets/Dialogs.kv')


if __name__ == '__main__':
    Dialogs().run()