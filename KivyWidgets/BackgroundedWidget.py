from kivy.uix.widget import Widget
from kivy.graphics import Rectangle, Color

GREEN_BAR_COLOR =  (0.612, 0.757, 0.459, 1.000)


class BackgroundedWidget(Widget):
    background_color = GREEN_BAR_COLOR

    def __init__(self, **kwargs):
        super(BackgroundedWidget, self).__init__(**kwargs)

        with self.canvas:
            Color(self.background_color[0],
                  self.background_color[1],
                  self.background_color[2],
                  self.background_color[3]
            )

            self.rect = Rectangle(
                pos=self.pos,
                size=self.size
            )

        self.bind(
            pos=self.update_rect,
            size=self.update_rect,
        )

    def update_rect(self, *args):
        self.rect.pos = self.pos
        self.rect.size = self.size
        self.canvas.clear()
        with self.canvas:
            Color(self.background_color[0],
                  self.background_color[1],
                  self.background_color[2],
                  self.background_color[3])

            Rectangle(
                pos=self.pos,
                size=(self.width,self.height)
            )
