from kivy.uix.treeview import TreeView, TreeViewNode


class HIPTreeView(TreeView):

    def __init__(self, **kwargs):
        self.all_nodes = {}
        super(HIPTreeView, self).__init__(**kwargs)

    def add_node(self, node, parent=None):
        super(HIPTreeView, self).add_node(node, parent)

        if parent is not None:
            if parent in self.all_nodes:
                parent_level = self.all_nodes[parent]
                self.all_nodes[node] = parent_level+1
            else:
                self.all_nodes[node] = 0
        else:
            self.all_nodes[node] = 0

    def remove_node(self, node):
        super(HIPTreeView, self).remove_node(node)
        del self.all_nodes[node]

    def clear(self):
        max_level = 0
        leveled_nodes = {}
        if self.all_nodes is None:
            return
        if len(self.all_nodes) == 0:
            return

        for node, level in self.all_nodes.items():
            if level > max_level :
                max_level = level
                if level in leveled_nodes:
                    leveled_nodes[level].append(node)
                else:
                    leveled_nodes[level] = [node]

        if len(leveled_nodes) > 0:
            for cur_level in reversed(range(max_level+1)):
                nodes = leveled_nodes[cur_level]
                for node in nodes:
                    self.remove_node(node)









